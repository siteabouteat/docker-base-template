const express = require('express');
const axios = require('axios');

const { connectDb } = require('./helpers/db');
const { port, host, authApiUrl, db } = require('./configs');

const mongoose = require('mongoose');

const app = express();

const postSchema = new mongoose.Schema({
  title: String
});
const Post = mongoose.model('Post', postSchema);

const startServer = () => app.listen(port, async (err, res) => {
    console.log(`2. Server API starting api at ${host}:${port}`);
    function randomIntFromInterval(min, max) {
      return Math.floor(Math.random() * (max - min + 1) + min)
    }
    const rndInt = randomIntFromInterval(1, 10000)
    const newPost = new Post({ title: `This is new post ${rndInt}` });
    await newPost.save();
});

app.get('/', (req, res) => res.send('Our MAIN api is working correct.'));
app.get('/test', (req, res) => res.send('Test API page.'));

app.get('/currentUser', (req, res) => res.send('Test API page current user.'));

app.get('/getDataFromBaseApi', (req, res) => {
  res.json({
    isFetchFromApiServer: true,
    data: {
      id: 123,
      name: 'Эти данные получены с API Server',
    }
  });
});

app.get('/testwithcurrentuser', (req, res) => {
  axios.get(authApiUrl + '/currentUser').then((response) => {
    res.json({
      testwithcurrentuser: true,
      currentUserFromAuth: response.data
    });
  })
});

connectDb()
  .on('error', console.log)
  .on('disconnect', connectDb)
  .once('open', () => {
    console.log("1. API mongodb start success", db);
    startServer();
  });

import axios from 'axios';

import {useEffect, useState} from 'react';
import './App.css';

function App() {
  const [isFetch, setIsFetch] = useState(false);

  const fetchDataHandler = () => {
    setIsFetch(true);
  };

  useEffect(() => {
    if(!isFetch) return;
    (async () => {
      const {data} = await axios.get('/api/getDataFromBaseApi');
      console.log(data);
    })();
  }, [isFetch]);
  return (
    <div className="App">
      <h1>Hello world!!!</h1>
      <div>
        <button onClick={fetchDataHandler}>Fetch data</button>
      </div>
    </div>
  );
}

export default App;

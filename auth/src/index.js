const express = require('express');
const axios = require('axios');

const { connectDb } = require('./helpers/db');
const { port, host, apiUrl, db } = require('./configs');

const mongoose = require('mongoose');

const app = express();

const startServer = () => app.listen(port, async (err, res) => {
    console.log(`2. Server auth starting api at ${host}:${port}`);
  });

app.get('/', (req, res) => res.send('Our AUTH api is working correct.'));
app.get('/test', (req, res) => res.send('Test AUTH page.'));

app.get('/api/currentUser', (req, res) => {
  res.json({
    auth: true,
    currentUser: {
      id: 123,
      name: 'Sergey',
      email: 'mail@mail.ru',
    }
  });
});

app.get('/getDataFromApi', (req, res) => {
  console.log('apiUrl', apiUrl);
  axios.get(apiUrl + '/getDataFromBaseApi').then((response) => {
    res.json({
      msg: response.data,
    });
  })
});

connectDb()
  .on('error', console.log)
  .on('disconnect', connectDb)
  .once('open', () => {
    console.log("1. Auth mongodb start success", db);
    startServer();
  });

const mongoose = require('mongoose');
const { db } = require('../configs');

module.exports.connectDb = () => {
  mongoose.connect(db, {});
  return mongoose.connection;
}
